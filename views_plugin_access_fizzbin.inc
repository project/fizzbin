<?php

/**
 * Access plugin that provides permission-based access control.
 */
class views_plugin_access_fizzbin extends views_plugin_access {
  function access($account) {
    return views_check_perm($this->options['perm'], $account);
  }

  function get_access_callback() {
    return array('fizzbin_check_days', array($this->options['days']));
  }

  function summary_title() {
    $days = fizzbin_days();

    foreach ($this->options['days'] as $day) {
      $show[] = t($days[$day]);
    }

    return implode(', ', $show);
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['days'] = array('default' => array());

    return $options;
  }

  function options_form(&$form, &$form_state) {

    $form['days'] = array(
      '#type' => 'checkboxes',
      '#options' => fizzbin_days(),
      '#title' => t('Permission'),
      '#default_value' => $this->options['days'],
      '#description' => t('Only show this view to users on the specified days.'),
    );
  }

  function options_submit($form, &$form_state) {
    $form_state['values']['access_options']['days'] = array_filter($form_state['values']['access_options']['days']);
  }
}
