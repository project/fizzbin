<?php

/**
 * Implementation of hook_views_plugins().
 */
function fizzbin_views_plugins() {
  $plugins = array(
    'access' => array(
 /*     'parent' => array(
        'no ui' => TRUE,
        'handler' => 'views_plugin_access',
        'parent' => '',
      ),*/
      'fizzbin' => array(
        'title' => t('Fizzbin'),
        'help' => t('Access will be granted to users only on certain days.'),
        'handler' => 'views_plugin_access_fizzbin',
        'uses options' => TRUE,
        'help topic' => 'access-perm',
      ),
    ),
  );

  return $plugins;
}